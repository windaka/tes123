const { Users } = require('../models')
const { bcrypt } = require('bcrypt')
//const { user } = require('../route/authRoute')
const { jwt } = require('jsonwebtoken')
const { nanoid } = require('nanoid')

async function register(username, password) {
    const payload = {
        id: nanoid(),
        username,
        password: await bcrypt.hash(password, 5)
    }

    const result = Users.create(payload)
    return {
        id: result.id,
        username: result.username,
        token: jwt.sign({ id: result.id }, process.env.JWT_SECRET)
    }
}

async function login(username, password) {
    const user = await Users.findOne({
        where: { username }
    })
    if (await bcrypt.compare(password, user.password)) {
        return {
            id: user.id,
            username: user.id,
            token: jwt.sign({ id: result.id }, process.env.JWT_SECRET)
        }
    } else {
        return "wrong password"
    }
}

module.exports = {
    register,
    login
}
const { Articles } = require('../models')
const { nanoid } = require('nanoid')

function get(query) {
    return Articles.findAll({
        where: query,
    })
}

function add(data) {
    return Articles.create({
        id: nanoid(),
        ...data
    })
}

function edit(id, data) {
    return Articles.update(data, {
        where: { id }
    })
}

function remove(id) {
    return Articles.destroy(data, {
        where: { id }
    })
}

module.exports = {
    get,
    add,
    edit,
    remove
}


const oldUser = {
    firstName: "Winda"
}

const user = {
    lastName: "Kurnia",
    age: 24
}


const newUser = {
    ...oldUser,
    ...user
}

console.log(newUser);